import { Outlet } from 'react-router-dom';
import { NavBar } from './core/NavBar.tsx';
import { useTheme } from './core/store/useTheme.ts';
import { AuthInterceptor } from './shared/auth/AuthInterceptor.tsx';


function App() {

  const theme = useTheme(state => state.theme)


  console.log('render app')
  return (
    <>
      ....{theme} -
      <AuthInterceptor />
      <NavBar />
      <Outlet />
    </>
  )
}

export default App
