import { PropsWithChildren } from 'react';
import { Navigate } from 'react-router-dom';
import { isLogged } from './auth.api.ts';

export function PrivateRoute(props: PropsWithChildren) {
  return <div>
    {
      isLogged() ?
        props.children : <Navigate to="/login" />
    }
  </div>
}
