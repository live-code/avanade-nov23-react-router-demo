import { PropsWithChildren } from 'react';
import { isLogged } from './auth.api.ts';

export function IfLogged(props: PropsWithChildren) {

  if (!isLogged())
    return null

  return props.children
}
