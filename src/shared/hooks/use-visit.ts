export function useVisit() {

  function fn(url: string) {
    window.open(url)
  }

  return fn
}
