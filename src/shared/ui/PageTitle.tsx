import React, { PropsWithChildren } from 'react';

 function PageTitleFn(props: PropsWithChildren) {
  return <div className="text-center my-6">
    <h1 className="text-2xl">
      {props.children}
    </h1>
  </div>
}
export const PageTitle = React.memo(PageTitleFn)
