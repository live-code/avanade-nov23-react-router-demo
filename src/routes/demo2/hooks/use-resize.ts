import { useEffect, useState } from 'react';

export function useResize() {
  const [size, setSize] = useState({ w: window.innerWidth, h: window.innerHeight});

  useEffect(() => {

    let timeout: number;

    const fn = function() {
      clearTimeout(timeout)
      timeout = setTimeout(() => {
        setSize({ w: window.innerWidth, h: window.innerHeight})
      }, 500)
    }

    window.addEventListener('resize', fn)

    return () => {
      window.removeEventListener('resize', fn)
    }
  }, []);

  return size;
}

