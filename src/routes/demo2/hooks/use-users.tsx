import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../../model/user.ts';

export function useUsers() {
  const [users, setUsers] = useState<Partial<User>[]>([])
  const [selectedUser, setSelectedUser] = useState<Partial<User>>()
  const [showPanel, setShowPanel] = useState(false)

  useEffect(() => {
    axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then(res => {
        setUsers(res.data)
      })
  }, []);

  function selectUserHandler(user: Partial<User>) {
    setSelectedUser(user);
    setShowPanel(true)
  }

  function deleteUser(id: number) {
    axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(() => {
        setUsers(prev => prev.filter(u => u.id !== id ))
      })
  }

  function addUser(formData: Partial<User>) {
    axios.post<Partial<User>>(`https://jsonplaceholder.typicode.com/users/`, formData)
      .then((res) => {
        setUsers(prev => [...prev, res.data])
      })
  }

  function editUser(formData: Partial<User>) {
    axios.patch<Partial<User>>(`https://jsonplaceholder.typicode.com/users/${selectedUser?.id}`, formData)
      .then((res) => {
        setSelectedUser(res.data)
        setUsers(prev => prev.map(u => {
          return u.id === selectedUser?.id ? res.data : u;
        }))

        /*
        const cloned = structuredClone(users)
        const index = users.findIndex(u => u.id === selectedUser?.id)
        cloned[index] = res.data
        setUsers(cloned)
         */
      })
  }
  function clearForm() {
    setSelectedUser(undefined)
    setShowPanel(true)
  }

  function closePanel() {
    setShowPanel(false)
  }


  return {
    users,
    selectedUser,
    showPanel,
    actions: {
      selectUserHandler,
      deleteUser,
      addUser,
      editUser,
      clearForm,
      closePanel,
    }

  }
}
