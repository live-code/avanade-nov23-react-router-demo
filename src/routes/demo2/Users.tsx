import { useEffect } from 'react';
import { useVisit } from '../../shared/hooks/use-visit.ts';
import { TotalUsers } from './components/TotalUsers.tsx';
import { UsersForm } from './components/UsersForm.tsx';
import { UsersList } from './components/UsersLIst.tsx';
import { useResize } from './hooks/use-resize.ts';
import { useUsers } from './hooks/use-users.tsx';

export function Users() {
  const {
    users, selectedUser, showPanel, actions
  } = useUsers();

  const visit = useVisit()
  const size = useResize()

  return <div>
    <button className="btn" onClick={actions.clearForm}>Add New</button>

    <button onClick={() => visit('http://www.fabiobiondi.dev')}>website</button>
    <UsersForm
      selectedUser={selectedUser}
      onAddUser={actions.addUser}
      onEditUser={actions.editUser}
      show={showPanel}
      onClose={actions.closePanel}
    />

    <UsersList
      data={users}
      onDeleteUser={actions.deleteUser}
      onSelectedUser={actions.selectUserHandler}
    />

    <TotalUsers data={users} />

    <div>w: {size.w}, h: {size.h}</div>
  </div>
}

// Custom hook
// PATCH / EDIT

