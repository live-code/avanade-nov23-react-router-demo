import React from 'react';
import { User } from '../../../model/user.ts';

interface UserListItemProps {
  user: Partial<User>;
  onDeleteUser: (id: number) =>  void;
  onSelectedUser: (user: Partial<User>) =>  void;

}

export const UserListItem = React.memo((props: UserListItemProps) => {
  const {
    user: {
      id, name, email = 'not defined'
    },
    onSelectedUser,
    onDeleteUser
  } = props;


  return (
    <li key={id} onClick={() => onSelectedUser(props.user)}>
      {name} - {email}
      <button className="btn"
              onClick={() => onDeleteUser(id!)}>Delete</button>
    </li>
  )
})
