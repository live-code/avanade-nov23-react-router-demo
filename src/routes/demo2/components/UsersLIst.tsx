import { User } from '../../../model/user.ts';
import { UserListItem } from './UsersListItem.tsx';

interface UsersListProps {
  data: Partial<User>[];
  onSelectedUser: (user: Partial<User>) =>  void;
  onDeleteUser: (id: number) =>  void;
}

export function UsersList(props: UsersListProps) {
  const { data, onDeleteUser, onSelectedUser } = props;
  return (
    <ul>
      {
        data.map(u => {
          return <UserListItem
            key={u.id}
            user={u}
            onDeleteUser={onDeleteUser}
            onSelectedUser={onSelectedUser}
          />
        })
      }
    </ul>
  )
}
