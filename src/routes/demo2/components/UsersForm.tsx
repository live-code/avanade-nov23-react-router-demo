import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { User } from '../../../model/user.ts';

interface UsersFormProps {
  selectedUser: Partial<User> | undefined;
  onAddUser: (data: Partial<User>) => void
  onEditUser: (data: Partial<User>) => void
  show: boolean;
  onClose: () => void;
}

const initialState = { name: '', email: ''};

export function UsersForm(props: UsersFormProps) {
  const {selectedUser, onAddUser, onEditUser} = props;
  const [formData, setFormData] = useState<Partial<User>>(initialState)

  useEffect(() => {
    if (selectedUser)
      setFormData(selectedUser)
    else {
      setFormData(initialState)
    }
  }, [selectedUser]);

  function changeHandler(event: React.ChangeEvent<HTMLInputElement>) {
    const value = event.currentTarget.value;
    const key = event.currentTarget.name;
    setFormData(prev => ({ ...prev, [key]: value})  )
  }

  const isNameValid = formData.name && formData.name.length > 1;
  const isEmailValid = formData.email && formData.email.length > 1;
  const isFormValid = isNameValid && isEmailValid

  function submitHandler(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (selectedUser) {
      onEditUser(formData)
    } else {
      onAddUser(formData)
    }
    props.onClose();
  }

  return (
    <div
      className={clsx(
        'bg-gray-200 w-64 fixed top-0 bottom-0  p-3 shadow-2xl border-2 border-l-white transition-all duration-1000',
        { '-right-64': !props.show},
        { 'right-0': props.show},
      )}

    >
      <form onSubmit={submitHandler} className="flex flex-col">

        <div className="flex justify-between">
          <div>{selectedUser ? `EDIT ${selectedUser.name}` : 'ADD NEW USER'}</div>
          <button onClick={props.onClose} type="button">❌</button>
        </div>
        <br/>
        <input type="text" name="name" placeholder="name" value={formData.name} onChange={changeHandler}/>
        <input type="text" name="email" placeholder="email" value={formData.email} onChange={changeHandler}/>
        <button className="btn"  type="submit" disabled={!isFormValid}>
          {selectedUser ? 'EDIT' : 'ADD'}
        </button>

      </form>
    </div>
  )
}
