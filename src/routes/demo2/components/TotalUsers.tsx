import { User } from '../../../model/user.ts';

export function TotalUsers(props: { data: Partial<User>[]}) {
  return <div>
    Total Users: {props.data.length}
  </div>
}
