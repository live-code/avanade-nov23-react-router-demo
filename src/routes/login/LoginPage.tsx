import { useNavigate } from 'react-router-dom';
import { useTheme } from '../../core/store/useTheme.ts';
import { signIn } from '../../shared/auth/auth.api.ts';
import { PageTitle } from '../../shared/ui/PageTitle.tsx';

export function LoginPage() {
  const navigate = useNavigate()
  const changeTheme = useTheme(state => state.changeTheme)
  const changeColor = useTheme(state => state.changeColor)

  function signInHandler() {
    signIn('fabio', '123')
    navigate('/admin')
  }

  console.log('render login')
  return <div>
    <PageTitle>LoginPage (simulation) </PageTitle>
    <input type="text"/>
    <input type="text"/>
    <button onClick={signInHandler}
            className="btn">SIgnIn</button>

    <button onClick={() => changeTheme('dark')}>Dark</button>
    <button onClick={() => changeTheme('light')}>Light</button>
    <button onClick={() => changeColor('red')}>red</button>
    <button onClick={() => changeColor('blue')}>blue</button>
  </div>
}
