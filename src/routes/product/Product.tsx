import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { User } from '../../model/user.ts';

export function Product() {
  const [user, setUser] = useState<User>()

  const params = useParams<{ productId: string}>();

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/users/' + params.productId)
      .then(res => {
          setUser(res.data)
      })
      .catch(e => {
        console.log('qui! errore')
      })
  }, []);

  return <div>
    Product
    {user ? user.name : null}
    {user && user.name}
    {user?.name}
  </div>
}
