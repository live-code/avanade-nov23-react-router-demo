import React, { PropsWithChildren, useCallback, useMemo, useState } from 'react';
import { PageTitle } from '../../../shared/ui/PageTitle.tsx';



export function Demo1bComposition() {
  const [counter, setCounter] = useState(0)
  const [name, setName] = useState('')

  const inc = useCallback(()  => {
    setCounter(c => c + 1)
  }, [])
  console.log('root')

  return (
    <div className="comp">
      <button onClick={() => setName('i'+Math.random())}>text random</button>
      <PageTitle> Composition </PageTitle>
      <Parent>
        <Child value={counter} increment={inc}/>
        <ChildInutile />
      </Parent>
    </div>
  )
}

const Parent = React.memo((props: PropsWithChildren) => {
  console.log(' parent')
  return <div className="comp">
    {props.children}
  </div>
})

const Child = React.memo(function (props: any) {
  console.log('  child')
  return <div className="comp">
    Child {props.value}

    <button onClick={props.increment} className="btn">+</button>

  </div>
})

// memoization
const ChildInutile = React.memo(() => {
  console.log('  ChildInutile')
  return <div className="comp">
    ChildInutile
  </div>
})
