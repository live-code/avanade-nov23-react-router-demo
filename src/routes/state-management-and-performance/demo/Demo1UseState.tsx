import React, { useCallback, useMemo, useState } from 'react';
import { PageTitle } from '../../../shared/ui/PageTitle.tsx';


// RENDER e COMMIT

const memory = new Set();

export function Demo1UseState() {
  const [counter, setCounter] = useState(0)
  const [name, setName] = useState('')

  const inc = useCallback(() => {
    setCounter(c => c + 1)
  }, [])

  function setRandomText() {
    setName('item' + Math.random())
  }

  const calc = useMemo(
    () => {
      console.log(counter)
      return counter * 10
    },
    [counter]
  )

  memory.add(inc)
  console.log('root', memory)

  return <div className="comp">

   <PageTitle>demo useCallback, useMemo e React memo</PageTitle>

    Root {counter}

    Esempio usememo {calc}

    <button onClick={setRandomText} className="btn">Random Text {name}</button>
    <Parent value={counter} increment={inc} />
  </div>
}

const Parent = React.memo((props: any) => {
  console.log(' parent')
  return <div className="comp">
    Parent {props.value}
    <Child value={props.value} increment={props.increment}/>
    <ChildInutile />
  </div>
})


const Child = React.memo(function (props: any) {
  console.log('  child')
  return <div className="comp">
    Child {props.value}

    <button onClick={props.increment} className="btn">+</button>

  </div>
})

// memoization
const ChildInutile = React.memo(() => {
  console.log('  ChildInutile')
  return <div className="comp">
    ChildInutile
  </div>
})
