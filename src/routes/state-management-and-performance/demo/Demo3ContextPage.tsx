import React, { createContext, memo, PropsWithChildren, useCallback, useContext, useState } from 'react';
import { PageTitle } from '../../../shared/ui/PageTitle.tsx';

interface Config {
  counter: number;
  text: string;
}

const Demo3Context = createContext<Config | null>(null)
const ThemeContext = createContext<'dark' | 'light'>('light')

export function Demo3ContextPage() {
  const [data, setData] = useState<Config>({ counter: 0, text: ''})
  const [theme, setTheme] = useState<'dark' | 'light'>('dark')

  const inc = useCallback(()  => {
    setData(prevState => ({ ...prevState, counter: prevState.counter + 1 }))
  }, [])
  const setRandomName = useCallback(()  => {
    setData(prevState => ({ ...prevState, text: 'i' + Math.random() }))
  }, [])

  return (
    <Demo3Context.Provider value={data}>
      <ThemeContext.Provider value={theme}>
        <PageTitle> Context </PageTitle>
        <button className="btn" onClick={() => setTheme('dark')}>dark</button>
        <button className="btn" onClick={() => setTheme('light')}>light</button>
        <button className="btn" onClick={inc}>+ {data.counter}</button>
        <button className="btn" onClick={setRandomName}>random text</button>

        <Parent />
      </ThemeContext.Provider>
    </Demo3Context.Provider>)
}


const Parent = React.memo(() => {
  console.log(' parent')
  return <div className="comp">
   <SubParent />
  </div>
})


const SubParent = React.memo(() => {
  console.log(' subparent')
  return <div className="comp">
    <Child />
    <Child2 />
    <ChildInutile />
  </div>
})


const Child = React.memo(function (props: any) {
  console.log('  child1')
  const state = useContext(Demo3Context)
  return <div className="comp">
    Child1 {state?.counter}

    <button onClick={props.increment} className="btn">+</button>

  </div>
})


const Child2 = React.memo(function (props: any) {
  console.log('  child2')
  const state = useContext(Demo3Context)
  return <div className="comp">
    Child2 {state?.text}

    <button onClick={props.increment} className="btn">+</button>

  </div>
})

// memoization
const ChildInutile = React.memo(() => {

  console.log('  ChildInutile')
  const state = useContext(ThemeContext)

  return <div className="comp">
    ChildInutile
    Child {state?.text}
  </div>
})
