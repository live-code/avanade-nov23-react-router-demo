import { NavLink, Outlet } from 'react-router-dom';

export function StateManagementAndPerformancePage() {
  return <div>
    <h1 className="text-2xl my-6">State Management</h1>

    <NavLink to="use-state" className="btn">use state</NavLink>
    <NavLink to="composition" className="btn">composition</NavLink>
    <NavLink to="use-reducer" className="btn">use reducer</NavLink>
    <NavLink to="context" className="btn">context</NavLink>

    <hr/>
    <Outlet />
  </div>
}
