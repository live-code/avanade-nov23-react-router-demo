import { NavLink, Outlet } from 'react-router-dom';

export function AdminPage() {
  return <div>

    <NavLink to="page1" className="btn">Page 1</NavLink>
    <NavLink to="/admin/page2" className="btn">Page 2</NavLink>
    <hr/>

    <Outlet />
  </div>
}
