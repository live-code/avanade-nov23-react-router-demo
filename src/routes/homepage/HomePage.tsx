import axios from 'axios';

export function HomePage() {

  async function load() {
    const res = await axios.get('https://jsonplaceholder.typicode.com/users')
    console.log(res)
  }
  return <div>
    <h1>HomePage</h1>

    <button onClick={load}>Load</button>
  </div>
}
