import { NavLink, useLoaderData } from 'react-router-dom';
import { Post } from '../../model/post.ts';
import { User } from '../../model/user.ts';

interface Data {
  users: User[];
  posts: Post[]
}

export function Products() {
  const data = useLoaderData() as Data

  return <div className="flex">

    <div className="w-1/2">
      <h1>Users</h1>
      {
        data.users.map(p => {
          return <li key={p.id}>
            <NavLink to={`/products/${p.id}`}>
              {p.name}
            </NavLink>
          </li>
        })
      }
    </div>

    <div className="w-1/2">
      <h1>Posts</h1>
      {
        data.posts.map(p => {
          return <li key={p.id}>
            <NavLink to={`/posts/${p.id}`}>
              {p.title}
            </NavLink>
          </li>
        })
      }
    </div>
  </div>
}
