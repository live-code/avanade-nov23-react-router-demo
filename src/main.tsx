import axios from 'axios';
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, Navigate, RouterProvider } from 'react-router-dom';
import App from './App.tsx'
import './index.css'
import { AdminPage } from './routes/admin/AdminPage.tsx';
import { Page1 } from './routes/admin/components/Page1.tsx';
import { Page2 } from './routes/admin/components/Page2.tsx';
import { LoginPage } from './routes/login/LoginPage.tsx';
import { HomePage } from './routes/homepage/HomePage.tsx';
import { PostDetails } from './routes/post/Post.tsx';
import { Products } from './routes/products/Products.tsx';
import { Product } from './routes/product/Product.tsx';
import { Demo1bComposition } from './routes/state-management-and-performance/demo/Demo1BUseStateComposition.tsx';
import { Demo1UseState } from './routes/state-management-and-performance/demo/Demo1UseState.tsx';
import { Demo2UseReducer } from './routes/state-management-and-performance/demo/Demo2UseReducer.tsx';
import { Demo3ContextPage } from './routes/state-management-and-performance/demo/Demo3ContextPage.tsx';
import {
  StateManagementAndPerformancePage
} from './routes/state-management-and-performance/StateManagementAndPerformancePage.tsx';
import { PrivateRoute } from './shared/auth/PrivateRoute.tsx';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: 'home',
        element: <HomePage />
      },
      {
        path: 'demo2',
        async lazy() {
          const { Demo2 } = await import('./routes/demo2/Demo2.tsx')
          return { Component: Demo2}
        }
      },
      {
        path: 'products1',
        element: <Products />,
        loader: () => {
          return fetch('https://jsonplaceholder.typicode.com/users')
        }
      },
      {
        path: 'products',
        element: <Products />,
        loader: async () => {
          const users = await axios.get('https://jsonplaceholder.typicode.com/users')
          const posts = await axios.get('https://jsonplaceholder.typicode.com/posts')
          return {
            users: users.data,
            posts: posts.data
          }
        }
      },
      {
        path: 'products/:productId',
        element: <Product />,
      },
      {
        path: 'posts/:postId',
        element: <PostDetails />,
        loader: (obj) => {
          return fetch(`https://jsonplaceholder.typicode.com/posts/${obj.params.postId}`)
        }
      },
      {
        path: 'login',
        element: <LoginPage />
      },
      {
        path: 'admin',
        element: <PrivateRoute><AdminPage /></PrivateRoute>,
        children: [
          {
            path: 'page1',
            element: <Page1 />
          },
          {
            path: 'page2',
            element: <Page2 />
          },
          {
            path: '',
            element: <Navigate to="page1" />
          }

        ]
      },
      {
        path: 'state-management',
        element: <StateManagementAndPerformancePage />,
        children: [
          { path: 'use-state', element: <Demo1UseState /> },
          { path: 'composition', element: <Demo1bComposition /> },
          { path: 'use-reducer', element: <Demo2UseReducer /> },
          { path: 'context', element: <Demo3ContextPage /> },
          { path: '', element: <Navigate to="use-state" /> },
        ]
      },
      {
        path: 'demo3',
        element: <div>
          <h1>Demo 3</h1>
        </div>,
      },
      {
        path: '',
        element: <Navigate to="home" />
      }
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')!).render(
  <div>
    <RouterProvider router={router} />
  </div>
)
