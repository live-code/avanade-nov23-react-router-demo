import { NavLink, useNavigate } from 'react-router-dom';
import { logout } from '../shared/auth/auth.api.ts';
import { IfLogged } from '../shared/auth/IfLogged.tsx';


export function NavBar() {
  const navigate = useNavigate();

  function logoutHandler() {
    logout()
    navigate('/login');
  }

  return (
    <div className="flex flex-wrap gap-2 p-3 border-b-2 ">
      <NavLink className="btn" to="/">Homepage</NavLink>
      <NavLink className="btn" to="demo2">demo2</NavLink>
      <NavLink className="btn" to="products">products</NavLink>
      {/*<NavLink className="btn" to="demo3">demo3</NavLink>*/}
      <NavLink className="btn" to="login">login</NavLink>
      <NavLink className="btn " to="state-management">State</NavLink>

      <IfLogged>
        <NavLink className="btn" to="admin">admin</NavLink>
      </IfLogged>

      <IfLogged>
        <button className="btn" onClick={logoutHandler}>logout</button>
      </IfLogged>
    </div>
  )
}
