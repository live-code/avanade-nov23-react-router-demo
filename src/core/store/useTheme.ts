import { create } from 'zustand';

export type Theme = 'dark' | 'light' ; // union type + literal types

export type ThemeState = {
  theme: Theme;
  color: string;
  changeColor: (val: string) => void;
  setDark: () => void;
  setLight: () => void;
  changeTheme: (theme: Theme) => void;
}

export const useTheme = create<ThemeState>((set) => ({
  theme: 'dark',
  color: 'black',
  setDark: () => set({ theme: 'dark'}),
  setLight: () => set({ theme: 'light'}),
  changeTheme: (theme: Theme) => set({ theme }),
  changeColor: (val: string) => set({ color: val }),
}))
